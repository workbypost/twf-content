Locationimage: 

- 
  image: >
    http://127.0.0.1:9000/content/locations/1-aungier-street-hostel/800x500.png

----

Locationtitle: 

Aungier 
Street Hostel

----

Where: Where

----

Address: 

55 Aungier Street,
Dublin 2

----

Reviews: Reviews

----

Reviewcontent: (link: http://tripadvisor.com text: Tripadvisor)

----

Contact: Contact

----

Contactnumber: (tel: 00 353 1 475 0001)

----

Contactemail: (email: info@whitefriar.ie)

----

Rooms: Rooms From

----

Price: €20.00

----

Title: Aungier Street Hostel

----

When: When

----

Dates: 02/06/2016 - 12/09/2016

----

Location: 

- 
  image: >
    http://twf.bypost.webfactional.com/content/4-locations/1-aungier-street-hostel/location_pic.jpg
  locationtitle: 'Aungier</br>Street Hostel'
  title1: Where
  address: '55 Aungier Street,</br>Dublin 2'
  title2: Contact
  number: '(tel: 00 353 1 475 0001)'
  email: '(email: info@whitefriar.ie)'
  title3: Reviews
  link: '(link: http://wikipedia.org text: Tripadvisor)'
  title4: Rooms From
  price: "20.00"